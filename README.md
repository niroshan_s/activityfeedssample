# README #

To run this application you'll need to run gradle bootRun in both ActivityFeedBuilder and ActivityFeedGenerator

### What is this repository for? ###

* This is a very simple prototype of how to build an activity feed using a few spring technologies.

### How do I get set up? ###

* You'll need to download and run activeMq in order to run this. This application uses the default ports so no additional requirements are needed.
* Run gradle bootRun in both ActivityFeedBuilder and ActivityFeedGenerator

This project is just a simple prototype I built to get familiar with the spring framework.
