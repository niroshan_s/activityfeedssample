package com.niro.activityfeed.builder;

import com.niro.activityfeed.generator.ActivityFeedItem;
import org.springframework.stereotype.Service;

/**
 * Created by Niro on 8/8/2015.
 */
@Service
public class ActivityFeedStream implements ActivityFeedItemObserver {


    @Override
    public void onActivityFeedItemEvent(ActivityFeedItem activityFeedItem) {
        System.out.println("Add this to a stream: " + activityFeedItem);
    }
}
