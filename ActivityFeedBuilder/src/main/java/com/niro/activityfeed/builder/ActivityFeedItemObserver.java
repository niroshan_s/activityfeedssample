package com.niro.activityfeed.builder;

import com.niro.activityfeed.generator.ActivityFeedItem;

/**
 * Created by Niro on 8/8/2015.
 */
public interface ActivityFeedItemObserver {

    void onActivityFeedItemEvent(ActivityFeedItem activityFeedItem);

}
