package com.niro.activityfeed.builder;

import com.niro.activityfeed.generator.ActivityFeedItem;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by Niro on 8/1/2015.
 */
@Component
public class ActivityFeedJMSReceiver {

    private final ConfigurableApplicationContext context;
    private final List<ActivityFeedItemObserver> observers;


    @Inject
    ActivityFeedJMSReceiver(ConfigurableApplicationContext context, List<ActivityFeedItemObserver> observers) {
        this.context = context;
        this.observers = observers;
    }

    @JmsListener(destination = "activity-feed")
    public void reciveMessage(ActivityFeedItem activityFeedItem) {
        System.out.println("Received " + activityFeedItem);

        for (ActivityFeedItemObserver observer : observers) {
            observer.onActivityFeedItemEvent(activityFeedItem);
        }



    }


}
