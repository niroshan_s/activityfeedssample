package com.niro.activityfeed.builder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.jms.annotation.EnableJms;

/**
 * Created by Niro on 7/26/2015.
 */
@SpringBootApplication
@EnableJms
@Import(value = {ActivityFeedBuilderConfig.class})
public class ActivityFeedBuilderApplication {


    public static void main(String[] args) {
        SpringApplication.run(ActivityFeedBuilderApplication.class);
    }


}
