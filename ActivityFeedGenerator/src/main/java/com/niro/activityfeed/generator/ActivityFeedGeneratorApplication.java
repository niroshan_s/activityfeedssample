package com.niro.activityfeed.generator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Created by Niro on 7/26/2015.
 */
@SpringBootApplication
@EnableScheduling
@EnableJms
@Import(value = {ActivityFeedConfig.class})
public class ActivityFeedGeneratorApplication {


    public static void main(String[] args) {
        SpringApplication.run(ActivityFeedGeneratorApplication.class);
    }


}
