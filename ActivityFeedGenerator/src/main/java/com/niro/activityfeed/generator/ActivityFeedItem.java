package com.niro.activityfeed.generator;

import java.io.Serializable;

/**
 * Created by Niro on 7/26/2015.
 */
public class ActivityFeedItem implements Serializable {


    public enum ACTION {
        LIKES,
        COMMENTED,
        SHARED
    }


    private final String name;
    private final ACTION action;
    private final String item;

    public ActivityFeedItem(String name, ACTION action, String item) {
        this.name = name;
        this.action = action;
        this.item = item;
    }


    public String getName() {
        return name;
    }

    public ACTION getAction() {
        return action;
    }

    public String getItem() {
        return item;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ActivityFeedItem that = (ActivityFeedItem) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (action != that.action) return false;
        return !(item != null ? !item.equals(that.item) : that.item != null);

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (action != null ? action.hashCode() : 0);
        result = 31 * result + (item != null ? item.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ActivityFeedItem{" +
                "name='" + name + '\'' +
                ", action=" + action +
                ", item='" + item + '\'' +
                '}';
    }
}
