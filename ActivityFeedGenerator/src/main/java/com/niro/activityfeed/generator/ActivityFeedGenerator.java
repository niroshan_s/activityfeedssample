package com.niro.activityfeed.generator;

/**
 * Created by Niro on 7/26/2015.
 */
public class ActivityFeedGenerator {

    private static final String[] names = new String[]{"Bob", "Jill", "Steve"};


    private static final String[] items = new String[]{"google.ca", "youtube.com", "reddit.com"};

    public static ActivityFeedItem generateActivityFeedItem() {
        String name = names[getRandom()];
        ActivityFeedItem.ACTION action = ActivityFeedItem.ACTION.values()[getRandom()];
        String item = items[getRandom()];

        return new ActivityFeedItem(name, action, item);
    }

    private static int getRandom() {
        return new Double((Math.random() * 100) % 3).intValue();
    }


}
