package com.niro.activityfeed.generator;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

/**
 * Created by Niro on 7/26/2015.
 */
@Component
public class ActivityFeedGeneratorScheduler {

    private final ConfigurableApplicationContext context;

    @Inject
    ActivityFeedGeneratorScheduler(ConfigurableApplicationContext context) {
        this.context = context;
    }


    @Scheduled(fixedRate = 500)
    public void generateActivity() {
        ActivityFeedItem activityFeedItem = ActivityFeedGenerator.generateActivityFeedItem();


        MessageCreator messageCreator = getMessageCreator(activityFeedItem);
        JmsTemplate jmsTemplate = context.getBean(JmsTemplate.class);

        System.out.println("Sending " + activityFeedItem);

        jmsTemplate.send("activity-feed", messageCreator);

    }

    private MessageCreator getMessageCreator(ActivityFeedItem activityFeedItem) {
        return (session) -> session.createObjectMessage(activityFeedItem);
    }


}
